export interface IbankSlipResponse {
  barCode: string,
  amount: string,
  expirationDate: string
}
