import { Request, Response } from 'express'
import VerifyBankSlipService from '../services/VerifyBankSlipService';
import { container } from 'tsyringe';
import { IbankSlipResponse } from '../interface/IbankSlip';

class BankSlipController {

  public async show(request: Request, response: Response): Promise<any> {
    const  { code }  = request.params;
    
    try {
      const verifyBankSlipService = container.resolve( VerifyBankSlipService ) ;
      code.replace(/[^0-9]/g, '');
      const bankSlip:IbankSlipResponse = await verifyBankSlipService.execute({ code })
      
      return response.json(bankSlip);
    } catch (err:any) {
      return response.status(400).json({ error: err.message });
    }

  }
}

export default new BankSlipController()
