import VerifyBankSlipService from "./VerifyBankSlipService"
var verifyBankSlipService:any

describe('VerifyBankSlipService', () => {
  
  beforeEach(() => {
    verifyBankSlipService = new VerifyBankSlipService() ;

  })

  it('call identifyCodeType success', async () => {
    
     const response =  await verifyBankSlipService.identifyCodeType("21290001192100012109047561740597570000002000");
    
     expect(response).toBeTruthy();
  })
})
