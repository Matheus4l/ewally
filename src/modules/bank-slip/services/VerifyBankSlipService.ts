import { IbankSlipResponse } from '../interface/IbankSlip';
import moment from 'moment-timezone';
interface Request {
  code: string;
}

class VerifyService {


  public async execute({ code }: Request): Promise<IbankSlipResponse> {

    try {
      await this.identifyCodeType( code )
      
      return {
        "barCode": await this.convertTobarcode( code ),
        "amount": `R$ ${await this.identifyValue( code )}`,
        "expirationDate": await this.identifyDate( code )
      }
    } catch (error: any) {
      throw error;
    }
  }

  public async identifyCodeType(code: string): Promise<any> {

    try {
      if (code.length >= 46 || code.length <= 48) {
        return true
      } else {
        throw new Error('TAMANHO_INCORRETO')
      }
    } catch (error: any) {
      throw error
    }
  }
  public async identifyDate(code: any): Promise<any> {

    try {
      let fatorData = '0';
      let dataBoleto = moment.tz("1997-10-07 20:54:59.000Z", "UTC");
      fatorData = code.substr(10, 7)
      
      dataBoleto.add(Number(fatorData), 'days');

      return  dataBoleto.format('L')
    } catch (error: any) {
      throw error;
    }
  }

  public async identifyValue(code: any) {

    try {

      let valueBankSlip = code.substr(37);
      let valueFinal:any = valueBankSlip.substr(0, 8) + '.' + valueBankSlip.substr(8, 2);

      let char = valueFinal.substr(1, 1);
      while (char === '0') {
        valueFinal = await this.substringReplace(valueFinal, '', 0, 1);
        char = valueFinal.substr(1, 1);
      }

      return  parseFloat(valueBankSlip);
    } catch (error: any) {
      throw error;
    }
  }

  public async convertTobarcode(code: any){

    try {
        code = code.split('');
        code.splice(11, 1);
        code.splice(22, 1);
        code.splice(33, 1);
        code.splice(44, 1);
        code = code.join('');

        const result = code;

      return result;
    } catch (error: any) {
      throw error;
    }
  }


  public async substringReplace(str: any, repl: any, inicio: any, tamanho: any){

    try {
      if (inicio < 0) {
        inicio = inicio + str.length;
      }

      tamanho = tamanho !== undefined ? tamanho : str.length;
      if (tamanho < 0) {
        tamanho = tamanho + str.length - inicio;
      }

      return [
        str.slice(0, inicio),
        repl.substr(0, tamanho),
        repl.slice(tamanho),
        str.slice(inicio + tamanho)
      ].join('');
    } catch (error: any) {
      throw error;
    }
  }
}

export default VerifyService;
