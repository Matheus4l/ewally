import { Router } from 'express';
import banckSlip from './banckSlip.route';


const routes = Router();
routes.use('/boleto', banckSlip);

export default routes;
