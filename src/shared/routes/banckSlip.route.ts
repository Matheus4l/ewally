import { Router } from 'express';
import BankSlipController from '../../modules/bank-slip/controlles/BankSlipController';

const productRouter = Router();

productRouter.get('/:code', BankSlipController.show);

export default productRouter;
