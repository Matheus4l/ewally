
# Inicando o Projeto no terminal

Execute na pasta raiz e execute o seguinte comando
```
yarn dev:server
```

# Inicando o Projeto com o Docker
Será necessário  que o docker e o docker-compose estaja previamente instalado na maquina

https://docs.docker.com/engine/install/

https://docs.docker.com/compose/install/


Execute na pasta raiz e execute os seguintes comandos
```
docker-compose build --no-cache
```
```
docker-compose up -d
```

//Route da api 
```
[ http://localhost:3333/boleto/21290001192100012109047561740597570000002000 ]  post
```
Exemplo de retorno 
{
	"barCode": "21290001192000121090476174059757000002000",
	"amount": "R$ 2000",
	"expirationDate": "05/28/7747"
}
}


# Obs:

A aplicação estará rodando http://localhost:3333



